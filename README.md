# Taller de edición digital

Repositorio del Taller de Edición Digital.

## Donaciones

🌮 Dona para unos tacos con [ETH](https://etherscan.io/address/0x39b0bf0cf86776060450aba23d1a6b47f5570486).

:dog: Dona para unas croquetas con [DOGE](https://dogechain.info/address/DMbxM4nPLVbzTALv5n8G16TTzK4WDUhC7G).

:beer: Dona para unas cervezas con [PayPal](https://www.paypal.me/perrotuerto).

## Páginas hermanas

Taller de Edición Digital: [ted.perrotuerto.blog](https://ted.perrotuerto.blog/).

_Edición digital como metodología para una edición global_: [ed.perrotuerto.blog](https://ed.perrotuerto.blog/).

Pecas, herramientas editoriales: [pecas.perrotuerto.blog](https://pecas.perrotuerto.blog/).

Blog: [perrotuerto.blog](https://perrotuerto.blog).

## Licencia

Todos los escritos están bajo [Licencia Editorial Abierta y Libre 
(LEAL)](https://github.com/NikaZhenya/licencia-editorial-abierta-y-libre).
