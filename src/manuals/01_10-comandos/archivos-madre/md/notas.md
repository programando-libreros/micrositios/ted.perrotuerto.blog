Por ejemplo: `man ls`. Con esto le indicarás a la terminal que te
muestre el _man_ual del comando `ls`. Bien puedes optar por `ls --help`,
que te mostrará la ayuda de `ls`, por poner otro ejemplo. Cabe resaltar
que para desplegar la ayuda, si `--h` no funciona, seguro `--help` servirá.

Cygwin no cuenta con `nano` preinstalado, por lo que se tiene que instalar
desde su gestor de paquetes.

El directorio _home_ de Cygwin puede no ser el mismo al _home_ de Windows.
Depende de la configuración realizada al instalarlo. Si no puedes encontrar
el _home_ de Cygwin, con probabilidad se encontrará en el disco local
`C`. De ahí busca la instalación de Cygwin.

Existe un comando más ilustrativo llamado `tree`. Si lo tienes instalado 
puedes ejecutar `tree borrame` para que veas una vista en árbol del 
fichero `borrame`. ¿Más cómodo, cierto?

¿Sabías que por pereza no hay necesidad de escribirlo todo? Si usas
la tecla `Tab` es posible autocompletar las palabras. Así tendrás un
uso más ágil de la terminal y es muy útil para saber hacia dónde
estás retrocediendo.

El editor `nano` es de los más recientes. En la tradición linuxera
existen otros editores con más peso como `vim` o +++GNU+++ Emacs.
Pero este manual es básico, así que empleamos `nano` por ser el más
sencillo.

Por convención la tecla `Ctrl` se simboliza con `\^`. Así, `^X` es 
igual a `Ctrl + X`.

¿Sabías que el formato +++ODT+++ es el archivo abierto para los
documentos de texto procesado? Aunque el +++DOCX+++ o el +++DOC+++ han 
dominado el panorama, The Document Foundation vela por la implementación 
de estándares abiertos para la paquetería de oficina y +++ODT+++ es 
uno de sus formatos.

El ejemplo usa exclusivamente `cat`, pero el operador `>` puede usarse
para cualquier comando.

Al hacer saltos de línea con `\`, el símbolo `>` lo agrega Bash de 
manera automática.
