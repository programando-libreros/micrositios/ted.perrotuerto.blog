<section epub:type="chapter" role="doc-chapter">

# Breve historia de la edición digital

La producción de publicaciones con *software* ha variado con el tiempo 
y se ha basado en métodos que tienen enfoques muy particulares sobre el 
tratamiento del texto. Un breve recorrido histórico quizá puede ayudar a 
visibilizar una serie de eventos que nos han llevado a la manera actual de 
componer documentos de manera digital.

![Mundo procesado. ¿Estás procesando o te han procesado?](../img/s02_img01.jpg)

## Recursos

1. «[Historia de la edición digital](http://marianaeguaras.com/historia-de-la-edicion-digital/)».
2. «[Visión general del sistema GNU](https://www.gnu.org/gnu/gnu-history.es.html)».
3. [*El manifiesto de GNU*](https://www.gnu.org/gnu/manifesto.es.html).
4. [*La catedral y el bazar*](http://biblioweb.sindominio.net/telematica/catedral.html).
5. «[Today in Apple history: Apple’s war with IBM commences](https://www.cultofmac.com/441919/apple-history-ibm-personal-computer/)».
6. «[Microsoft’s Relationship with Apple](http://www.mac-history.net/apple/2011-01-30/microsofts-relationship-with-apple)».
7. «[Por qué el “código abierto” pierde de vista lo esencial del software libre](https://www.gnu.org/philosophy/open-source-misses-the-point.html)».

----

## Conceptos claves

Existen algunos términos fundamentales que ayudarán en este breve recorrido
histórico:

* «*Hardware*»
* «*Software*»
* «Código máquina»
* «Código fuente»
* «Sistema operativo»
* «Interfaz gráfica de usuario / [GUI]{.versalita}»
* «Licencias de uso»
* «Editor de texto»
* «Editor de código»
* «Procesador de texto»
* «Ofimática / automatización de oficinas»
* «Composición de textos»
* «Diagramación / maquetación»
* «*Desktop publishing*»
* «Propiedad intelectual»

## Lucha de titanes del *software* propietario

En los años ochenta empieza el *boom* de las computadoras personales y, con 
ello, la popularización del tratamiento digital del texto. Lo que en la
actualidad se entiende por «computadora personal» fue en gran parte definido
por la lucha entre [IBM]{.versalita}, Apple y Microsoft.

### IBM *vs* Apple

La batalla se dio en el campo del *hardware*:

* Apple y su Apple II.
* [IBM]{.versalita} y su [IBM PC]{.versalita}
* Microsoft se mantiene al margen como proveedora de *software*; 
  colabora con [IBM]{.versalita} para un nuevo sistema operativo —[DOS]{.versalita}— de lo que sería la [IBM PC DOS]{.versalita};
  colabora con Apple al permitirle usar su versión de [BASIC]{.versalita} —un lenguaje de programación— para la Apple II.

> Microsoft de manera inteligente no vende su *software* sino que se vale de la
> gestión de la propiedad intelectual para «licenciarlo».

### Apple *vs* Microsoft

La batalla se llevó a cabo en el campo del *software*:

* Apple apuesta —y falla— por una computadora con entorno gráfico: Apple Lisa.
* Microsoft apuesta —y gana— por la creación de un sistema operativo con entorno gráfico: Windows.
* [IBM]{.versalita} mantiene el dominio del *hardware* con su [IBM PC]{.versalita}, 
  pero la mina de oro se muestra en el lado del *software* con Microsoft a la cabeza.

> Microsoft demuestra que el modelo de negocios en la industria de las 
> computadoras personales es el *software*.

### *Desktop publishing*

El concepto de *desktop publishing* viene de una evolución del tratamiento 
digital del texto que implica:

* La relevancia del *software* para el ámbito editorial.
* El uso de una interfaz gráfica para diagramar un texto.
* La gestión de la propiedad intelectual para el licenciamiento del *software*.

El camino puede rastrearse desde dos vertientes: la evolución de la máquina de
escribir y la automatización en la oficina, y la evolución de las computadoras.

![Máquina de escribir de +++IBM+++.](../img/s02_img02.jpg)

En la evolución de la máquina de escribir:

1. En los sesenta un trabajador de [IBM]{.versalita} forja el concepto de «procesador de texto»,
   con el cual se busca reemplazar a las máquinas de escribir mediante un 
   dispositivo físico —*hardware*— que enmiende uno de los principales problemas
   que se tenían en las oficinas: la falta de eficiencia de la máquina de 
   escribir por su incapacidad de reversibilidad y la relativa alta capacitación
   que se requería de su operador.
2. La máquina procesadora de texto es un gran fracaso, principalmente por su 
   alto costo.

En la evolución de las computadoras:

1. En los inicios de las computadoras digitales solo era posible programarlas
   con código binario: el código máquina.
2. Por la necesidad de facilitar el trabajo de programación surgen los lenguajes
   de programación para escribir código más entendible por humanos: el código fuente.
3. La escritura de código fuente implica la creación de un *software* para su
   redacción: el editor de texto.
4. Con el incremento en la popularidad de las computadoras se crea un editor de
   texto específico para la programación: el editor de código.
5. Con la entrada de las computadoras personales se requiere de la creación de
   programas para el usuario promedio y para funciones administrativas, por lo
   que se retoma la idea del «procesador de texto» para crear un editor de texto
   específico para la creación de documentos simples o administrativos.
6. Los procesadores de texto no tienen el calado para componer textos con 
   calidad editoriales y el aprendizaje de un lenguaje de marcado o de 
   programación va más allá de la enseñanza tradicional del diseñador gráfico,
   por lo que surge la necesidad de un programa con entorno gráfico para la
   maquetación de documentos: el *desktop publishing*.
7. El *desktop publishing* es desarrollado por compañías privadas, por lo que su
   adquisición convencional es mediante el pago de una licencia de uso.
8. En 2003 nace la primera y única alternativa aún viva y libre de *desktop publishing*: [Scribus](https://www.scribus.net/).

## La historia olvidada del *software* libre o de código abierto

No todas las personas involucradas en el desarrollo de *software* les agradó
lo que la lucha de titanes empezó a constituir como un hábito: ver el *software*
como una propiedad a restringir.

### Surgimiento del *software* libre

Recorrido:

* En 1983 surge el movimiento del *software* libre, cuyo principal representante es Richard Stallman.
* En 1984 surge el proyecto [GNU]{.versalita} con la intención de crear un sistema operativo «libre».
* En 1985 se publica *El manifiesto [GNU]{.versalita}* que define los lineamientos básicos del movimiento.

La «libertad» se entiende de cuatro maneras:

1. Libertad de usar el programa.
2. Libertad de estudiar el programa.
3. Libertad de distribuir el programa.
4. Libertad de mejorar el programa.

Estas cuatro libertades implican el acceso al código fuente de los programas, 
que para evitar su «cierre», es necesaria una licencia de uso que obligue a
todas las mejoras a permanecer «libres»: la licencia [GPL]{.versalita}.

### La bifurcación del código abierto

A finales de los noventa empieza una fuerte discusión dentro del movimiento del
*software* libre por las siguientes cuestiones:

* El término «libre» puede prestarse a muchas interpretaciones cuando en realidad
  lo que se busca es la «apertura» del código.
* El proyecto [GNU]{.versalita} tiene un fuerte carácter social, ético y normativo, 
  cuando la mayoría de las personas involucradas solo tiene el interés de usar el
  código y mejorar sus habilidades como programadores.
* La licencia [GPL]{.versalita} es demasiado restrictiva e impide trabajar en
  proyectos donde también está involucrado el código privativo.

Por estos motivos empieza a constituirse la iniciativa del código abierto:

* En 1997 se publica *La catedral y el bazar*, escrito por Eric S. Raymond y
  considerado un texto fundamental para la iniciativa.
* En 1998 un grupo de programadores se separa del movimiento del *software* libre
  para constituir la iniciativa del código abierto.

En el código abierto no se habla más de «libertad» sino de:

* «Apertura» del código para su uso: menos ética y más pragmatismo.
* Una práctica en lugar de una norma: Linux como ejemplo de este modelo.
* Un modelo colaborativo y al vuelo, sea o no para un beneficio económico, sea o 
  no con *software* libre o privativo, en lugar de uno jerarquizado y planificado.
* Una práctica que busca evitar hilos negros y quizá aliarse con corporaciones
  para agilizar el trabajo.

### TeX

El sistema de tipografía TeX se desarrolla de manera paralela al movimiento del
*software* libre y la iniciativa del código abierto, aunque su modelo de 
desarrollo comparte varias similitudes.

Recorrido:

* A principios de los setenta Donald E. Knuth se sintió insatisfecho por la baja
  calidad editorial de su obra *El arte de programar ordenadores*.
* En 1978 Knuth empieza el desarrollo de TeX, un lenguaje de programación y de
  marcado para la composición de textos.
* En 1984 se empieza un macro para facilitar el uso de TeX, LaTeX entra en escena
  como el modo más popular de trabajar con TeX.
* En 1989 se termina el desarrollo de TeX, aunque hubo liberaciones intermedias.
* En 1991 nace otro macro, ConTeXt, que permite el uso nativo de archivos [XML]{.versalita}.

![Manejo de bibliografíacon TeX.](../img/s02_img03.jpg)

TeX es altamente configurable pero para un usuario promedio presenta estas desventajas:

1. La composición de textos no es visual, sino estructural, por lo que tiene una
   muy baja adopción entre diseñadores gráficos y el mundo editorial —excepto en la 
   publicación académica de ciencias físico-matemáticas o ingenierías.
2. Su lenguaje es de marcado y de programación, por lo que su curva de aprendizaje
   es mayor al uso de un entorno gráfico para la maquetación de textos.
3. Al ser *software* libre, no existe un interés publicitario de su comunidad de
   desarrolladores, por lo que su acercamiento al usuario promedio no es comparable
   a la publicidad de grandes desarrolladores de *software* para el quehacer
   editorial como Adobe.
4. Al ser desarrollado de manera colaborativa, el grueso de sus usuarios están
   familiarizados con los lenguajes de marcado y programación, por lo que la creación
   de una interfaz amigable no es una prioridad —aunque su excepción es [LyX](http://www.lyx.org/).

## Proyecto colectivo para este módulo

¿Qué nos queda? Dos modelos de composición de textos:

1. El visual cuyo origen son empresas de *software*.
2. El estructural cuyo origen es una comunidad de desarrolladores.

Ambos modelos presentan profundas diferencias en el cuidado editorial 
que se verán en las próximas sesiones. También tienen sus diferencias en cuanto
la idea entorno al *software* que puede resumirse de la siguiente manera:

El modelo general y predominante en la edición digital ve al *software* como:

* Un producto corporativo y privado que se vende a través de licencias de uso.
* Un programa donde la interfaz gráfica es esencial.
* Un método visual que por lo general se orienta a la producción de un solo formato.

El modelo minoritario y muchas veces desdeñado que percibe al *software* como:

* Un producto abierto hecho por una comunidad que cualquiera, sin ninguna clase de permiso, puede utilizar.
* Un programa donde su uso a través de la terminal tiende a ser la prioridad.
* Un método mediante lenguajes de marcado que se orienta a la producción multiformato y a la automatización.

Nosotros nos orientaremos a la vertiente minoritaria, cuyo primer acercamiento
para este módulo se tiene la siguiente propuesta de trabajo práctico:

* Crear una antología en formato [EPUB]{.versalita}, [MOBI]{.versalita}, [PDF]{.versalita} digital
  y [PDF]{.versalita} para impresión.
* Cada uno de ustedes estaría al cuidado de un capítulo —cuidado editorial y en el formato.
* Cada capítulo su formato base será Markdown.
* Para los interesados en la digitalización, la recomendación sería que su sección venga de un texto impreso.
* Cada uno hará de manera automatizada las versiones [EPUB]{.versalita} y [MOBI]{.versalita}.
* Las versiones [PDF]{.versalita} y los diseños de forros las hará el tallerista.
* La portada puede ser creada por alguno de ustedes, por un tercero o por el tallerista.
* Al final del ciclo de edición los asistentes subirán las versiones digitales a distintos sitios y
  el tallerista cuidará la impresión del trabajo.

</section>
